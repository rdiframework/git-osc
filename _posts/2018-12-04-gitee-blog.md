---
layout: post
title: "Team@OSC 将不再维护，请导出数据到码云企业版"
---

<p>这是第二次正式发布通知（<a href="https://www.oschina.net/news/100145/team-will-move-to-gitee-enterprise">第一次</a>），我们计划在春节前正式关闭 Team@OSC，请大家尽快将 Team 迁移到码云企业版。</p><p>目前我们已经给大家准备了任务和文档的导入导出功能。</p><p>任务导入：</p><p><img src="https://oscimg.oschina.net/oscnet/5ab31731eff4ed984825fba8b678f9f2dfd.jpg" width="600"/></p><p>文档导入：</p><p><img src="https://oscimg.oschina.net/oscnet/6307fd75ee5ce19eb7dbdd32e3d46e0304e.jpg" width="300"/></p><p>详细的使用方法请看&nbsp;<a href="https://www.oschina.net/question/2267325_2290183">https://www.oschina.net/question/2267325_2290183</a></p><p>为了更好的使用体验，请尽快迁移，对您造成的不便，深表歉意。</p><p>最近码云企业版大优惠，购买多年折扣多。更多关于码云企业版的介绍请看&nbsp;<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>