---
layout: post
title: "码云企业版任务管理更新 —— 自定义任务类型和状态"
---

<p>为了不断满足企业开发中的管理诉求，码云企业版之任务管理再次更新，这次我们给大家带来了完全可自定义的任务类型和状态，可以为不同类型的任务定制不同的状态，满足个性化管理的需求。</p><p>如下图所示：</p><p><img alt="" height="293" src="https://oscimg.oschina.net/oscnet/29f8005e88c2e3180d60bc532c1a633f5f2.jpg" width="800"/></p><p>可以通过企业版的任务管理中进入个性设置：</p><p><img alt="" height="312" src="https://oscimg.oschina.net/oscnet/080c84649575ce222965cc878eb9660cf3f.jpg" width="600"/></p><p>下一步我们将对任务统计、看板的管理进一步的改进和优化，以满足各种开发管理的需求。</p><p>此外，因为此次任务管理升级在程序设计上跟老版本变化甚大，同时也为了简化产品的设计，我们合并之前老版本的&quot;已验收&quot; 状态，导致了部分使用使用该状态进行任务管理的企业在使用上造成了诸多的不便，我们在这里给受影响的企业版客户道歉。目前我们已经完全恢复了&quot;已验收&quot;状态，日后的更新我们会慎重评估的用户使用习惯，尽可能降低对用户已有习惯的影响。</p><p>助力企业开发管理、欢迎大家使用码云企业版&nbsp;<a href="https://gitee.com/enterprises?from=osc-news-task1">https://gitee.com/enterprises</a></p>