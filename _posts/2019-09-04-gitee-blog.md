---
layout: post
title: "码云 Gitee 增加对 Jupyter Notebook 的渲染支持"
---

<p>Jupyter Notebook（此前被称为 IPython notebook）是一个交互式笔记本，支持运行 40 多种编程语言。</p>

<p>Jupyter Notebook 的本质是一个 Web 应用程序，便于创建和共享文学化程序文档，支持实时代码，数学方程，可视化和&nbsp;markdown。 用途包括：数据清理和转换，数值模拟，统计建模，机器学习等等。</p>

<p>现在 Jupyter Notebook 被广泛用于机器学习、人工智能领域。</p>

<p>码云 Gitee 近期增加了对 Jupyter Notebook 文档 (.ipynb) 的渲染支持，渲染效果如下所示</p>

<p><img alt="" height="825" src="https://static.oschina.net/uploads/space/2019/0904/075408_3AB5_12.jpg" width="800" /></p>

<p>你可以通过以下地址查看实际渲染效果：</p>

<p><a href="https://gitee.com/ModelArts/ModelArts-Lab-gitee/blob/master/notebook/DL_image_recognition/image_recongition.ipynb">https://gitee.com/ModelArts/ModelArts-Lab-gitee/blob/master/notebook/DL_image_recognition/image_recongition.ipynb</a></p>
