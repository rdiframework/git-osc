---
layout: post
title: "码云新增 Fork 到组织或企业功能"
---

当你在码云上 Fork 项目的时候可能有过这种烦恼：想要 Fork 的项目和自己已有项目同名时会无法 Fork。基于用户使用过程中的这个功能痛点，码云对 Fork 功能进行了改造，本次对Fork功能进行了改进：用户可以选择将项目 Fork 到个人、组织、企业或者企业团队名下。


![输入图片说明](https://gitee.com/uploads/images/2017/1109/192039_52a375ab_927373.png "屏幕截图.png")


赶快来试试吧 -> [https://gitee.com](https://gitee.com)
