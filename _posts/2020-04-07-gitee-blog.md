---
layout: post
title: "Gitee 企业版上线 DevOps 工具「Gitee Go」，限时免费使用中"
---

<p>Gitee 企业版现已上线<strong> </strong><strong>DevOps&nbsp;工具「Gitee Go」</strong>。</p>

<p>用户可以通过「Gitee Go」自定义构建流程，实现构建集成自动化，目前已支持 Maven、Gradle、NPM、Python、Ant、PHP、Golang等工具和语言的持续构建与集成能力。&nbsp;</p>

<p><img alt="" height="371" src="https://images.gitee.com/uploads/images/2020/0421/133350_2b835565_551147.png" width="700" /></p>

<p>现「Gitee Go」已开启<strong>限时免费使用</strong>，即日起至2020 年 4 月 30 日，所有 Gitee 企业版用户均可免费体验「Gitee Go」带来的高效交付体验，快来试试吧~（每个企业限定配额 2000 分钟）</p>

<p>点此查看更详细的「Gitee Go」配置与使用方法：<a href="https://gitee.com/help/articles/4293" target="_blank">https://gitee.com/help/articles/4293</a></p>
