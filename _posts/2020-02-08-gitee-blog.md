---
layout: post
title: "远程工作一周，你的效率怎样？"
---

<p>这周是全国很多公司远程办公的第一周，这一周之内<a href="https://my.oschina.net/javaeye/blog/3163486">你被 @ 了多少次</a>呢？你在家办公会穿睡衣吗？<s>你会化妆吗？（不好意思，我忘了本站没有女程序员）。</s></p>

<p><img src="https://pic1cdn.cmbchina.com/cmbpic/202002/a3c454dd-e4b0-46bd-9766-ca9ebd0ca0e8-w835-h469.jpg" /></p>

<p>铲屎官们会被你家的猫搞得毫无脾气吗？</p>

<p>停停停！ 咱们还是严肃点！</p>

<p>咱们还是好好来分析一下，在家办公的这一周你的工作效率如何，和平时正常上班比较孰高孰低呢？</p>

<p>进入 Gitee 企业版 ，找到成员 -&gt; 成员贡献统计，选择时间段</p>

<p><img height="1464" src="https://oscimg.oschina.net/oscnet/up-f04a8c5c4a76512dada9b52fce4d9c701a8.png" width="2452" /></p>

<p>再找到平时工作周看看数据，对比一下吧？ 差多少？ 80% 以上算你牛！</p>

<p>非开发人员的话，你只能靠你的火眼金睛来判断了。</p>

<p>档燃了，代码量、任务数并不是全面评估工作效率的唯一手段，但对开发人员来说是一个很重要的可量化的角度。</p>

<p>开发人员说：请不要有事没事 @ 我了，自己上 <a href="https://gitee.com/enterprises">Gitee 企业版</a>看。</p>

<p>==============</p>

<p>另外我们在正在进行<a href="https://www.oschina.net/news/113152/remote-cooperation-bad-guide">远程办公的吐槽大赛</a>，参与评论得码云大桌布啦。</p>
