---
layout: post
title: "庆教师节，码云新增 PR 显示权限助力计算机教学"
---

<p>今天9月10日教师节，自6月份码云推出高校版以来，目前已经超过 300 个高校的上千名老师在使用码云高校版来管理计算机教学中的代码和文档的内容。</p><p>一般情况下开发者提交 PR 时，如果是公开项目那么这个 PR 是对所有人都可见的，如果是私有项目，PR 对项目组成员都可见。这样无法避免学生在提交作业或者代码比赛时相互借鉴的情况。</p><p>为了方便老师使用收集作业或者进行代码考试、比赛，码云平台新增功能 —— Pull Request 显示权限设置。</p><p>启用方式：[项目主页] -&gt; 管理 -&gt; 基本设置 -&gt; <strong>开启的 Pull Requests 显示权限</strong>。如下图所示：</p><p><img height="300" src="https://oscimg.oschina.net/oscnet/48a13bacc6933ed39aafab722db6205c153.jpg" width="654"/></p><p>一旦启用该设置，那么开发者提交的 PR 只有管理员、审查者以及测试者可见，其他开发者不可见。此功能主要用于高校版中，敦促学生独立完成作业。</p><p>更多关于码云高校版的信息，请访问&nbsp;<a href="https://gitee.com/education" _src="https://gitee.com/education">https://gitee.com/education</a></p>