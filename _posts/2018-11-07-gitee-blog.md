---
layout: post
title: "码云超轻量级 Gitee IDE 更新，新增暗色主题"
---

<p>上个月我们发布了轻量级在线编码环境 Gitee IDE，在过去近一个月的用户反馈中，我们做了些调整和更新。</p><p>相关更新功能描述如下：</p><p>- 支持黑暗主题（Dark mode），开发者可以更加的专注书写代码了。<br/>- 增加 Vue、Wxss、Wxml 等文件代码语法高亮。<br/>- 若干Bug修复</p><p><img alt="" height="530" src="https://oscimg.oschina.net/oscnet/7d588d2a1a9c189441828234ab765a6d3e8.jpg" width="1024"/></p><p>欢迎大家体验，可前往个人的项目仓库页面点击 Web IDE 按钮即可打开。</p><p>即刻体验 Gitee IDE <a href="https://gitee.com/-/ide/project/ld/J2Cache/edit/master">https://gitee.com/-/ide/project/ld/J2Cache/edit/master</a>。</p>